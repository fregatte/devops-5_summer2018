#!/usr/bin/env bash


TASK_DIR="./task"
USERS_ALL=(R1 R2 R3 R4 R5 L1 L2 L3 A1 A2 A3 A4)
PROJECTS=(Proj1 Proj2 Proj3)

if (( ${EUID} != 0 )); then
    echo "Error at checking permissions!"
    echo "Please run this script under root user or with sudo!"
    exit 1
fi

###########################################################
# Clear environment

for USERNAME in "${USERS_ALL[@]}"; do
    userdel -rf ${USERNAME} &>/dev/null
done

rm -rf ${TASK_DIR}

###########################################################
# Create new environment

for USERNAME in "${USERS_ALL[@]}"; do
    useradd ${USERNAME}
done

mkdir ${TASK_DIR}

for PROJECT in "${PROJECTS[@]}"; do
    mkdir ${TASK_DIR}/${PROJECT}
done

###########################################################
# Set ACLs on directories

setfacl -m g::- ${TASK_DIR}/*
setfacl -m o::- ${TASK_DIR}/*
setfacl -dm g::- ${TASK_DIR}/*
setfacl -dm o::- ${TASK_DIR}/*

###########################################################
# Set ACL on Proj1

USERS=(R2 R3 R5 A1)
for USERNAME in "${USERS[@]}"; do
    setfacl -dm u:${USERNAME}:rwx ${TASK_DIR}/Proj1/
    setfacl -m u:${USERNAME}:rwx ${TASK_DIR}/Proj1/
done

USERS=(A4)
for USERNAME in "${USERS[@]}"; do
    setfacl -dm u:${USERNAME}:rx ${TASK_DIR}/Proj1/
    setfacl -m u:${USERNAME}:rx ${TASK_DIR}/Proj1/
done

USERS=(L1 L2 L3)
for USERNAME in "${USERS[@]}"; do
    setfacl -m u:${USERNAME}:rwx ${TASK_DIR}/Proj1/
done

###########################################################
# Set ACL on Proj2

USERS=(R1 R5 A1)
for USERNAME in "${USERS[@]}"; do
    setfacl -dm u:${USERNAME}:rwx ${TASK_DIR}/Proj2/
    setfacl -m u:${USERNAME}:rwx ${TASK_DIR}/Proj2/
done

USERS=(A2 A3)
for USERNAME in "${USERS[@]}"; do
    setfacl -dm u:${USERNAME}:rx ${TASK_DIR}/Proj2/
    setfacl -m u:${USERNAME}:rx ${TASK_DIR}/Proj2/
done

USERS=(L1 L2 L3)
for USERNAME in "${USERS[@]}"; do
    setfacl -m u:${USERNAME}:rwx ${TASK_DIR}/Proj1/
done

###########################################################
# Set ACL on Proj3

USERS=(R1 R2 R4 A2)
for USERNAME in "${USERS[@]}"; do
    setfacl -dm u:${USERNAME}:rwx ${TASK_DIR}/Proj3/
    setfacl -m u:${USERNAME}:rwx ${TASK_DIR}/Proj3/
done

USERS=(A1 A4)
for USERNAME in "${USERS[@]}"; do
    setfacl -dm u:${USERNAME}:rx ${TASK_DIR}/Proj3/
    setfacl -m u:${USERNAME}:rx ${TASK_DIR}/Proj3/
done

USERS=(L1 L2 L3)
for USERNAME in "${USERS[@]}"; do
    setfacl -m u:${USERNAME}:rwx ${TASK_DIR}/Proj1/
done
