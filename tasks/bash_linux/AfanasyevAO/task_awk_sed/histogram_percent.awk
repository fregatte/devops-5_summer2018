BEGIN {
    for (i=0;i<11;i++) arr[i]=0
}

{
    arr[int($0/10)]++
}

END {
    for (i=0;i<11;i++)
	sum+=arr[i]

    for (i=0;i<11;i++)
	arr[i]=arr[i]/sum*100
    
    for (i=0;i<11;i++)
    {
	
	if ( i*10+9 < 100 )
	    printf("%3d - %2d: %4d%s ", i*10, i*10+9, arr[i], "%")
	else
	    printf("%3d%-6s %4d%s ",i*10, ":", arr[i], "%")
	for (j=0;j<arr[i];j++) printf("*")
	print("")
    }

}