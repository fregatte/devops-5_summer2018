# Usage

Run script:

```sh
$ ./worker.sh -s "/pub/incoming" -d "/pub/output" [-l "/var/log/my_logs"] [-r] [-f "YYYY-MM-DD hh:mm"] [-t "YYYY-MM-DD hh:mm"] [-a 365] [-h]
```

- `-s` Sets the directory for input files.
- `-d` Sets the directory for output files.
- `-l` Sets the directory for log files (default is the script's directory).
- `-r` Generate report which count files received.
- `-f` Sets "from" date for report (if omitted will be used all time before "to" date).
- `-t` Sets "to" date for report (if omitted will be used "now" date).
- `-a` Archive files which are in the destination directory for more than X days.
- `-h` Displays help message about usage script.
