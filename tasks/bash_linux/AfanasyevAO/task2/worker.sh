#!/usr/bin/env bash

# Unique pid for log filenames
TIMEPID=$(date +%s)
# Script filename
SCRIPT_NAME=$(basename -s .sh "$0")
# Directory from which the script is running
SCRIPT_PATH=$(dirname "$0")
# Default log directory
LOG_DIR="${SCRIPT_PATH}/logs"

###############################################################################

# Function for print message how to use the script
function USAGE {
    echo -e "\\nUsage: ${SCRIPT_NAME}.sh -s \"/pub/incoming\" -d \"/pub/output\" [-l \"/var/log/my_logs\"] [-r] [-f \"YYYY-MM-DD hh:mm\"] [-t \"YYYY-MM-DD hh:mm\"] [-a 365] [-h]\\n"
    echo -e "-s --Sets the directory for input files."
    echo -e "-d --Sets the directory for output files."
    echo -e "-l --Sets the directory for log files (default is the script's directory)."
    echo -e "-r --Generate report which count files received."
    echo -e "-f --Sets \"from\" date for report (if omitted will be used all time before \"to\" date)."
    echo -e "-t --Sets \"to\" date for report (if omitted will be used \"now\" date)."
    echo -e "-a --Archive files which are in the destination directory for more than X days."
    echo -e "-h --Displays this help message."
}

# Function for write log messages
function WR2LOG {

    LOGTIME=$(date +"%Y-%m-%d %H:%M:%S")
    echo "[${LOGTIME}] $1" >> "${LOG_FILENAME}"
}

# Function for process source file to destination
function PROCESS_FILE {
    # Get filename without path
    SRC_FILE=$(basename "$1")
    SRC_WORK_FILE=".${SRC_FILE}.in-progress"
    # Check that this file is not processing by another instance of our script
    [[ "${SRC_FILE}" =~ ^\..*\.in-progress$ ]] && return
    # Get last line of file as UUID
    OBJ_UUID=$(tail -n 1 "${SRC_DIR}/${SRC_FILE}" 2>/dev/null)
    # Check errors on read source file
    if [ $? -ne 0 ]; then
        return
    # Check that string has a correct UUID format
    elif [[ ! "${OBJ_UUID}" =~ ^\{?[A-F0-9a-f]{8}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{12}\}?$ ]]; then
        return
    fi
    # Rename file to hide it from another instances our script
    mv -f "${SRC_DIR}/${SRC_FILE}" "${SRC_DIR}/${SRC_WORK_FILE}" 2>/dev/null
    # Return if there are errors on rename file
    [ $? -ne 0 ] && return
    # Get first string from source file
    HEAD_STR=$(head -n 1 "${SRC_DIR}/${SRC_WORK_FILE}" 2>/dev/null)
    # Get file s last modification date
    OBJ_MOD_DATE=$(stat -c %y "${SRC_DIR}/${SRC_WORK_FILE}")
    # Change date format
    OBJ_MOD_DATE=$(date --date="${OBJ_MOD_DATE}" +"%Y%m%d-%H%M" 2>/dev/null)
    # Return if there is errors with date's converting
    [ $? -ne 0 ] && return
    # Get object name from string
    OBJ_NAME=$(echo "${HEAD_STR}" | cut -f 1)
    # Get object date from string
    OBJ_DATE=$(echo "${HEAD_STR}" | cut -f 2)
    # Change date format
    OBJ_DATE=$(date --date="${OBJ_DATE}" +"%Y%m%d-%H%M"  2>/dev/null)
    if [ $? -ne 0 ]; then
        return
    fi
    # Create destination filename
    DEST_FILE="${OBJ_NAME}--${OBJ_DATE}--${OBJ_MOD_DATE}"
    # Rename and move file to destination directory
    mv -bf "${SRC_DIR}/${SRC_WORK_FILE}" "${DEST_DIR}/${DEST_FILE}" 2>/dev/null
    if [ $? -eq 0 ]; then
        WR2LOG "File \"${SRC_FILE}\" moved as \"${DEST_FILE}\""
        touch -c "${DEST_DIR}/${DEST_FILE}"
    fi
}

###############################################################################

# Parse arguments from command line
while getopts ":s:d:l:f:t:a:rh" INPUT_ARGS; do
    case ${INPUT_ARGS} in
        s)  SRC_DIR="${OPTARG}"
            ;;
        d)  DEST_DIR="${OPTARG}"
            ;;
        l)  LOG_DIR="${OPTARG}"
            ;;
        r)  REPORT_GEN=1
            ;;
        f)  REPORT_FROM_DATE="${OPTARG}"
            ;;
        t)  REPORT_TO_DATE="${OPTARG}"
            ;;
        a)  if [[ "${OPTARG}" =~ ^[0-9]+$ ]]; then
                ARCHIVE_DAYS=${OPTARG}
            else
                echo "Invalid argument \"${OPTARG}\" for option \"-a\", its must be a number!" >&2
                USAGE
                exit 1
            fi
            ;;
        h)  USAGE
            exit 0
            ;;
        \?) echo "Invalid option \"-${OPTARG}\", please, check your syntax!" >&2
            USAGE
            exit 1
            ;;
    esac
done

# Check for required arguments
if [ -z "${SRC_DIR}" ] || [ -z "${DEST_DIR}" ]; then
    echo "Required parameter missing, check syntax!" >&2
    USAGE
    exit 1
fi

# Set filename for new log file
LOG_FILENAME="${LOG_DIR}/${SCRIPT_NAME}-${TIMEPID}.log"

# Create log directory if it doesn't exists
mkdir -p "${LOG_DIR}" &>/dev/null
if [ $? -ne 0 ]; then
    echo "Cannot access log directory, check path and ownership!" >&2
    exit 1
fi

# Check that log file is writable
> "${LOG_FILENAME}" &>/dev/null
if [ $? -ne 0 ]; then
    echo "Cannot write log file, check path and ownership!" >&2
    exit 1
fi

WR2LOG "Work started."
WR2LOG "Source files directory     : ${SRC_DIR}"
WR2LOG "Destination files directory: ${DEST_DIR}"
WR2LOG "Log files directory        : ${LOG_DIR}"

# Create destination directory if it doesn't exists
mkdir -p "${DEST_DIR}" &>/dev/null
if [ $? -ne 0 ]; then
    WR2LOG "Cannot access destination directory, check path and ownership!"
    exit 1
fi

# Processing files in source directory
for ENTRY in "${SRC_DIR}"/*; do
    [ -f "${ENTRY}" ] || continue
    PROCESS_FILE "${ENTRY}"
done

[ -z "${REPORT_FROM_DATE}" ] && REPORT_FROM_DATE="0001-01-01 00:00:00"
[ -z "${REPORT_TO_DATE}" ] && REPORT_TO_DATE=$(date "+%Y-%m-%d %H:%M:%S")

# Generate report if there is the parameter
if [ "${REPORT_GEN}" ]; then
    # Get list files in the destination directory
    for ENTRY in "${DEST_DIR}"/*; do
        # Check that we find only regular files
        [ -f "${ENTRY}" ] || continue
        # Get last line of file as UUID (for exclude
        # unwanted files in destination directory)
        OBJ_UUID=$(tail -n 1 "${ENTRY}" 2>/dev/null)
        # Check errors on read file
        if [ $? -ne 0 ]; then
            WR2LOG "Cannot read the file\"${ENTRY}\" while compiling report!"
            continue
        # Check that string has a correct UUID format
        elif [[ ! "${OBJ_UUID}" =~ ^\{?[A-F0-9a-f]{8}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{12}\}?$ ]]; then
            continue
        fi
        # Get first line from file
        HEAD_STR=$(head -n 1 "${ENTRY}" 2>/dev/null)
        # Get first word from first line
        OBJ_NAME=$(echo "${HEAD_STR}" | cut -f 1)
        # Check and compare age of file
        IN_REPORT=$(find "${ENTRY}" -maxdepth 1 -type f \
                                    -newermt "${REPORT_FROM_DATE}" \
                                    -not -newermt "${REPORT_TO_DATE}" 2>/dev/null)
        if [ "${IN_REPORT}" ]; then
            echo "${OBJ_NAME}"
        fi
        # Send cycle`s output to sort and uniq 
    done | sort | uniq -c | sort -n

fi

# Archive files if there is the parameter
if [ "${ARCHIVE_DAYS}" ]; then

    AGO_DATE=$(date --date="${ARCHIVE_DAYS} day ago" "+%Y-%m-%d %H:%M:%S" 2>/dev/null)
    # Collect an array with object's names from
    # all needed files in destination directory
    OBJ_NAMES=()
    for ENTRY in "${DEST_DIR}"/*; do
        # Check that we find only regular files
        [ -f "${ENTRY}" ] || continue
        # Check that file older than we want
        NEW_FILE=$(find "${ENTRY}" -maxdepth 1 -type f -newermt "${AGO_DATE}" 2>/dev/null)
        [ "${NEW_FILE}" ] && continue
        # Get last line of file as UUID (for exclude
        # unwanted files in destination directory)
        OBJ_UUID=$(tail -n 1 "${ENTRY}" 2>/dev/null)
        # Check errors on read file
        if [ $? -ne 0 ]; then
            WR2LOG "Cannot read the file\"${ENTRY}\" while creating archive!"
            continue
        # Check that string has a correct UUID format
        elif [[ ! "${OBJ_UUID}" =~ ^\{?[A-F0-9a-f]{8}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{12}\}?$ ]]; then
            continue
        fi
        # Get first line from file
        HEAD_STR=$(head -n 1 "${ENTRY}" 2>/dev/null)
        # Get first field from first line
        OBJ_NAME=$(echo "${HEAD_STR}" | cut -f 1)
        # Add object name to array
        OBJ_NAMES+=("${OBJ_NAME}")
    done

    # Generate an array of unique object's names
    mapfile -t OBJ_NAMES_UNIQ < <(for ENTRY in "${OBJ_NAMES[@]}"; do
                                        echo "${ENTRY}"
                                  done | sort -u)

    
    # Generate an array with the oldest dates for every unique object name
    LATEST_DATES=()
    for UNIQ_NAME in "${OBJ_NAMES_UNIQ[@]}"; do
        ARCH_TO_DATE="${AGO_DATE}"
        ARCH_FROM_DATE="${AGO_DATE}"
        for ENTRY in "${DEST_DIR}/${UNIQ_NAME}--"*; do
            IN_ARCHIVE=$(find "${ENTRY}" -maxdepth 1 -type f ! -newermt "${AGO_DATE}" 2>/dev/null)
            if [ "${IN_ARCHIVE}" ]; then
                FILE_MOD_DATE=$(stat -c %y "${ENTRY}")
                FILE_MOD_DATE_S=$(date --date="${FILE_MOD_DATE}" +"%s" 2>/dev/null)
                ARCH_FROM_DATE_S=$(date --date="${ARCH_FROM_DATE}" +"%s" 2>/dev/null)
                [ "${FILE_MOD_DATE_S}" -lt "${ARCH_FROM_DATE_S}" ] && ARCH_FROM_DATE="${FILE_MOD_DATE}"
            fi
        done
        LATEST_DATES+=("${ARCH_FROM_DATE}")
    done
    
    # Create archive for every unique object name
    for (( i=0; i < ${#OBJ_NAMES_UNIQ[@]}; i++ )); do
        
        UNIQ_NAME="${OBJ_NAMES_UNIQ[${i}]}"
        LATEST_DATE="${LATEST_DATES[${i}]}"

        ARCH_TO_DATE=$(date --date="${AGO_DATE}" +"%Y%m%d-%H%M" 2>/dev/null)
        ARCH_FROM_DATE=$(date --date="${LATEST_DATE}" +"%Y%m%d-%H%M" 2>/dev/null)
        ARCH_FNAME="${DEST_DIR}/${UNIQ_NAME}--${ARCH_FROM_DATE}--${ARCH_TO_DATE}.tar.gz"

        for ENTRY in "${DEST_DIR}/${UNIQ_NAME}"*; do
            OBJ_UUID=$(tail -n 1 "${ENTRY}" 2>/dev/null)
            if [[ ! "${OBJ_UUID}" =~ ^\{?[A-F0-9a-f]{8}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{12}\}?$ ]]; then
                continue
            fi
            IN_ARCHIVE=$(find "${ENTRY}" -maxdepth 1 -type f ! -newermt "${AGO_DATE}" 2>/dev/null)
            [ "${IN_ARCHIVE}" ] && echo "$(basename "${ENTRY}")"
        done | tar cfz "${ARCH_FNAME}" -C "${DEST_DIR}" --remove-files -T -
    done
fi

WR2LOG "Work done. Bye!"
