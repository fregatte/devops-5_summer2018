# Usage

### Run script by itself

```sh
$ ./template_engine.sh template.file output.file
```
 - template.file - Filename of template for processing 
 - output.file - Filename of output file for result

### Run script by "source" from another script

```sh
#!/usr/bin/env bash
...
source template_engine.sh
PROCESS_TEMPLATE template.file output.file
...
```

 -  PROCESS_TEMPLATE - function from template_engines.sh
 - template.file - Filename of template for processing 
 - output.file - Filename of output file for result

### Patterns in template file

You can use any kind of patterns `{%pattern%}` that will be replaced by the value of the corresponding Bash variables (if this variables exists and not empty). For naming patterns you can use any symbols except `{`,`}` brackets.
 
 - `{%UID%}`,`{%SHELL%}`,`{%MY_COOL_VAR%}` - Good patterns
 - `{%MY_{}_VAR%}`, `{WRONG%}` - Wrong patterns
