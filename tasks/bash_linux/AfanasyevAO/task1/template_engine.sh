#!/usr/bin/env bash

function PROCESS_LINE {
    # Process string
    TEMPLATE_STR=$1
    # Extract first template var {%VAR_NAME%} from string
    INPUT_VAR=$(echo "${TEMPLATE_STR}" | sed -ne 's/.*\({%.[^}]*%}\).*/\1/p')
    # Repeat, while template vars exists in string
    while [ "${INPUT_VAR}" ]; do
        # Remove brackets {% %} from var name
        INPUT_VAR=$(echo "${INPUT_VAR}" | cut -d'%' -f 2)
        # Get value from bash var
        VALUE=${!INPUT_VAR}
        # Escape slashes in value for sed
        VALUE=$(echo "$VALUE" | sed -e 's:\/:\\/:g')
        # Replace template var in string to value of bash var
        TEMPLATE_STR=$(echo "${TEMPLATE_STR}" | sed -ne "s/\(.*\)\({%.[^}]*%}\)\(.*\)/\1${VALUE}\3/p")
        # Find next template var in string
        INPUT_VAR=$(echo "${TEMPLATE_STR}" | sed -ne 's/.*\({%.[^}]*%}\).*/\1/p')
    done
}

function PROCESS_TEMPLATE {
    # Fill our variables from args
    TEMPLATE_FILE=$1
    OUT_FILE=$2

    # Check that variables exists
    if [ -z "${OUT_FILE}" ] || [ -z "${TEMPLATE_FILE}" ]; then
        echo "Required parameter missing, check syntax!" >&2
        exit 1
    fi

    # Check that template file is readable
    if [ ! -r "${TEMPLATE_FILE}" ]; then
        echo "Cannot read template file, check path and ownership!" >&2
        exit 1
    fi

    # Check that output file is writable
    $(> "${OUT_FILE}")
    if [ $? -ne 0 ]; then
        echo "Cannot write output file, check path and ownership!" >&2
        exit 1
    fi

    # Process lines in template file
    while read LINE; do
        PROCESS_LINE "${LINE}"
        echo "${TEMPLATE_STR}" >> "${OUT_FILE}"
    done < "${TEMPLATE_FILE}"
}


# If script running by itself we execute our function with passing args
RUN_NAME=$(basename $0)
SCRIPT_NAME=$(basename ${BASH_SOURCE})

if [ "${RUN_NAME}" == "${SCRIPT_NAME}" ]; then
    PROCESS_TEMPLATE "$@"
fi
