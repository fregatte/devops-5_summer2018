#!/usr/bin/env bash

WGET_INSTALLED=$(command -v wget)
APT_INSTALLED=$(command -v apt)
YUM_INSTALLED=$(command -v yum)

PERCONA_REPO="https://repo.percona.com/apt"
PERCONA_DEB="percona-release_0.1-6.$(lsb_release -sc)_all.deb"
PERCONA_PASS="12345678"
PERCONA_DEBCONF="percona-server-server-5.7 percona-server-server-5.7"
PHPMA_DEBCONF="phpmyadmin phpmyadmin"

if ! (( "${EUID}" == 0 )); then
    echo "Please run this script under root user or with sudo!"
    exit 1
else
    echo "- Running as root, ok"
fi

if ! [ "${WGET_INSTALLED}" ]; then
    echo "- Wget is not installed, cannot continue..." >&2
    exit 1
else
    echo "- Wget found, ok"
fi

if [ "${APT_INSTALLED}" ]; then
    echo "- Apt found, ok"
    echo -n "- Get keys and repository package as: \"${PERCONA_REPO}/${PERCONA_DEB}\""
    wget -q "${PERCONA_REPO}/${PERCONA_DEB}" -O "${PERCONA_DEB}" 2>/dev/null
    [ $? -ne 0 ] && { echo ", failed"; exit 1; } || echo ", ok"

    echo -n "- Installing package with keys and repos"
    dpkg -i "${PERCONA_DEB}" &>/dev/null
    [ $? -ne 0 ] && { echo ", failed"; exit 1; } || echo ", ok"

    echo -n "- Updating list repos and packages"
    apt update &>/dev/null
    [ $? -ne 0 ] && { echo ", failed"; exit 1; } || echo ", ok"

    echo -n "- Installing Percona MySQL Server"
    echo "${PERCONA_DEBCONF}/root-pass password" | debconf-set-selections &>/dev/null
    echo "${PERCONA_DEBCONF}/re-root-pass password" | debconf-set-selections &>/dev/null
    apt install -y percona-server-server-5.7 &>/dev/null
    [ $? -ne 0 ] && { echo ", failed"; exit 1; } || echo ", ok"

    echo -n "- Setting root password and security plugin in Percona MySQL Server"
    sleep 5
    mysql -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '${PERCONA_PASS}';"
    [ $? -ne 0 ] && { echo ", failed"; exit 1; } || echo ", ok"

    echo -n "- Installing phpMyAdmin"
    echo "${PHPMA_DEBCONF}/internal/skip-preseed boolean true" | debconf-set-selections &>/dev/null
    echo "${PHPMA_DEBCONF}/reconfigure-webserver multiselect apache2" | debconf-set-selections &>/dev/null
    echo "${PHPMA_DEBCONF}/dbconfig-install boolean false" | debconf-set-selections &>/dev/null
    apt install -y phpmyadmin &>/dev/null
    [ $? -ne 0 ] && { echo ", failed"; exit 1; } || echo ", ok"
    echo "- All task done, goodbye!"
else
    echo "- Apt is not installed, cannot continue..." >&2
    exit 1
fi

