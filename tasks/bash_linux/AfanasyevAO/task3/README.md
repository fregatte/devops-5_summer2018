# Usage

Run script:

```sh
$ sudo ./percona_install.sh
```
# Requirements

- sudo or root access
- Installed wget and apt packages

# Description

This script install Percona MySQL Server 5.7 and phpMyAdmin from repos.

After installation, phpMyAdmin can be accessed at `http://localhost/phpmyadmin/` with credentials `root/12345678`.